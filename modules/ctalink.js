import {ctaTool} from './ctatool.js';

Hooks.on('init', () => {

})

Hooks.on("createToken", async (token,options, userId) =>{

    if(CTA==null)
		return;
	if(!game.user.isGM)
		return;
	
	let tokenData = token.data;
	let originalActor = game.actors.get(tokenData.actorId);
	let citems = originalActor.data.data.citems;
	let changeActor = false;
	if(!originalActor.isToken)
		changeActor = true;
	
	let tokenObj = token;
	
	if(token.actor.isToken){
		if(tokenData.actorData.citems){
		citems = tokenData.actorData.citems;
		}
		
		
	}
	
	tokenObj = token._object;
	
	let anim = await ctaTool.createIcons(tokenObj,citems);	
	//console.log(anim);
});

Hooks.on("updateActor", async (actor,updateData,options,userId) =>{

    if(CTA==null)
		return;
	if(!game.user.isGM)
		return;
	
	let token;
	let changeActor = false;
	
	//console.log(actor);
	//console.log(updateData);
	
	if(actor.isToken){
		token = actor.token;
	}
	else{
		token = actor._sheet.token;
		
		if(token==null)
			token = canvas.tokens.placeables.find(y=>y.actor.id == actor.id)?.document;
		
		changeActor = true;
	}
	
	if(token==null)
		return;
	
	//console.log(token);
	
	let tokenData = token.data;
	let citems;
	let tokenObj = token;
	
	if(actor.isToken){
		if(tokenData.actorData.citems){
			citems = tokenData.actorData.citems;
		}
		else{
			citems = actor.data.data.citems;
		}
		
	}
	
	else{//TIENES UN PROBLEMA CON ESTE ABAJOQUE TE HACE UNA LOCURA DE LOOOOOP!!!
		if(updateData.data)
			if(updateData.data.citems)
				citems = updateData.data.citems;
	}
	
	//console.log("edit");
	//console.log(citems);
	
	if(citems!=null){
		tokenObj = token._object;
		
		let anim = await ctaTool.createIcons(tokenObj,citems);
		//console.log(anim);
		
		if(!hasProperty(updateData,"flags")){
			setProperty(updateData,"flags",{});
		}
		if(!hasProperty(updateData.flags,"Custom-Token-Animations")){
			setProperty(updateData.flags,"Custom-Token-Animations",{});
		}
		
		updateData.flags["Custom-Token-Animations"] = anim;
		//console.log(anim);	
	}
	
	

});


