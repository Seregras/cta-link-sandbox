import { auxMeth } from "/systems/sandbox/module/auxmeth.js";
export class ctaTool {
	static async createIcons(token,citems){
		//console.log("creating TOks");
		let changeActor = false;
		let noActor = token.actor.isToken;
		if(!noActor)
			changeActor=true;
			
		let coordX = 1.0;
		let textureDataArray = [];
		let names = [];
		for(let i=0;i<citems.length;i++){
			let citem = citems[i];
			let citemT = await auxMeth.getcItem(citem.id, citem.ciKey);
			//console.log(citemT.name);
			if(citemT.data.data.tokeniconpath!="" && citemT.data.data.istokenicon){
				//console.log("creando");
				let textureData = {
					texturePath: citemT.data.data.tokeniconpath,
					scale: "0.3",
					speed: 0,
					multiple: 1,
					rotation: "static",
					xScale: coordX,
					yScale: 0,
					belowToken: false,
					radius: 2,
					opacity: 1,
					tint: 16777215,
					equip: false,
					lock : false
				}
				
				if(citem.isactive || citem.usetype=="PAS" || citemT.data.data.tokenvisible){
					textureDataArray.push(textureData);
					await names.push(citemT.name);
				}
					
						
				coordX -=0.2;
			}
			
			
		}
		//console.log(names);
		//console.log(token);
	
		
		let anim;
		if(changeActor){
			//let actor = game.actors.get(token.actor.id);
			anim = token.actor.data.token.flags["Custom-Token-Animations"]?.anim;
		}
		else{
			anim = token.data.flags["Custom-Token-Animations"]?.anim;
		}
		
		//console.log(anim);
		//console.log(names);
		
		if(anim!=null){
			let nametoDelete = [];
			for(let j=0;j<anim.length;j++){
				let animflag = anim[j];
				nametoDelete.push(animflag.name);

			}
			//console.log(nametoDelete);
			if(nametoDelete.length>0){
				await CTA.removeAnimByName(token, nametoDelete, changeActor, true);
				anim = [];
			}
				
			
		}
		
		
		
		//console.log(names);
		
		if(names.length>0)
			await CTA.addAnimation(token, textureDataArray, changeActor, names, null);
		return anim;
				
	}
}